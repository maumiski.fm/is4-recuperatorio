// Nuestro NameSpace es "Recuperatorio". El namespace se utiliza como "contenedor" de clases.
// Para que se pueda utilizar las clases declaradas en el namespace en otros namespace y clases, utilizando la palabra reservada require
// También se utiliza para evitar conflictos entre los nombres de las clases, utilizando distintos namespace
namespace Recuperatorio;

// Acá estamos declarando una clase ejerccio
class Ejercicio {

    // Creamos una función RealizarEjercicio de la clase Ejercicio
    public function RealizarEjercicio() {
        echo "Este es el Tema 2 del recuperatorio.";
    }
}
