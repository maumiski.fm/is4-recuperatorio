-- Table: public.Usuario

-- DROP TABLE IF EXISTS public."Usuario";

CREATE TABLE IF NOT EXISTS public."Usuario"
(
    id integer NOT NULL DEFAULT nextval('"Usuario_id_seq"'::regclass),
    nombre text[] COLLATE pg_catalog."default",
    apellido text[] COLLATE pg_catalog."default",
    nombre_usuario text[] COLLATE pg_catalog."default",
    password text[] COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."Usuario"
    OWNER to postgres;