<?php
require_once __DIR__ . '/vendor/autoload.php';
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$db_user = $_ENV['DDBB_USER'];
$db_password = $_ENV['DDBB_PASSWORD'];
$db_host = $_ENV['DDBB_HOST'];
$db_port = $_ENV['DDBB_PORT'];

$conexion = "pgsql:host=$db_host;port=$db_port;dbname=gestión;user=$db_user;password=$db_password";

try {
    $pdo = new PDO($conexion);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = "select * from productos";

    $stmt = $pdo->query($query);

    echo "<table border='1'>
        <tr>
            <th>Producto</th>
        </tr>";

    while ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>
                <td>{$fila['nombre_producto']}</td>
            </tr>";
    }

    echo "</table>";
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}

$pdo = null;
?>
